/**
 * plugin.js
 *
 * Copyright, Moxiecode Systems AB
 * Released under MIT License.
 *
 * Author: DMI's Engineer
 *
 * License: http://opensource.org/licenses/MIT 
 * Contributing: https://bitbucket.org/sitdh/fedx 
 */

var window_width = 610,
    window_height= 500;
tinymce.PluginManager.add('fedx', function(editor) {
    editor.addButton('fedx', {
        type: 'button',
        text: 'คลังทรัพยากร',
        icon: false,
        tooltip: 'แทรกเนื้อหาจากคลังทรัพยากร',
        onclick: function() {
            tinyMCE.activeEditor.windowManager.open({
                title: "ค้นหาข้อมูลจากคลังทรัพยากรการศึกษาแบบเปิด",
                file: tinyMCE.baseURL + '/plugins/fedx/oer-image-list.html',
                width: window_width,
                height: window_height
            });
        }
    });
});
