tinyMCE ซึ่งเป็นเครื่องมือสำหรับแก้ไขข้อความแบบ Rich text บนเว็บไซต์ ที่เปิดให้นักพัฒนาทั่วไปสามารถพัฒนาส่วนขยายเพื่อเพิ่มเติมความสามารถตามที่ต้องการได้ โดย **FedX** คือส่วนขยายสำหรับ tinyMCE ที่ใช้สำหรับเชื่อมต่อกับ[คลังทรัพยากรการศึกษาแบบเปิด](http://oer.learn.in.th "คลังทรัพยากรการศึกษาแบบเปิด") เพื่อดึงข้อมูลรูปภาพ เอกสาร หรือวีดีโอ มาประกอบเนื้อหาบทเรียนที่สร้่างขึ้นใน[ระบบสื่อสาระออนไลน์แบบเปิด](http://oer.learn.in.th "ระบบสื่อสาระออนไลน์แบบเปิด")

## วิธีการติดตั้ง ##
1. [ดาวน์โหลดเวอร์ชั่นล่าสุด](https://bitbucket.org/sitdh/fedx/downloads)(ดูได้จากแถบ ```tags```) 
1. แตกไฟล์ที่ดาวน์โหลดลงมาจากนั้นจึงเปลี่ยนชื่อเป็น ```fedx```
1. คัดลอกโฟลเดอร์นี้ไปวางไว้ที่ "```/edx/app/edxapp/edx-platform/common/static/js/vendor/tinymce/js/tinymce/plugins/```" 
1. บรรทัดที่ 76 เพิ่มชื่อปลั๊กอิน (```fedx```) ต่อท้ายเข้าไปในไฟล์ ```editor.coffee``` (```/edx/app/edxapp/edx-platform/cms/static/xmodule_js/src/html/edit.coffee```) เพื่อเปิดการใช้งานปลั๊กอิน  
```plugins: "textcolor, link, image, codemirror, fedx"```
1. บรรทัดที่ 82 ใส่คำว่า ```fedx``` เข้าไปในตำแหน่งที่ต้องการให้ส่วนขยายนี้แสดงบนแทบเครื่องมือ ซึ่งในที่นี้จะวางไว้ที่ตำแหน่งขวาสุดของแถบเครื่องมือ
```toolbar: "formatselect | fontselect | bold italic underline forecolor wrapAsCode | bullist numlist outdent indent blockquote | link unlink image | code fedx"```

หมายเหตุ:  
* หากนำส่วนขยายนี้ไปติดตั้งบน edX Fullstack จำเป็นต้อง compile เนื้อหาทั้งหมดใหม่อีกครั้งด้วยคำสั่งดังนี้  
```bash
$ sudo -H -u edxapp bash   
# source /edx/app/edxapp/edxapp_env  
# cd /edx/app/edxapp/edx-platform  
# paver update_assets cms --settings=aws
```

* หลังจากคอมไพล์ asset เรียบร้อยแล้วให้สั่งเริ่มการทำงานของ LMS และ CMS (หรือ Studio) ใหม่ด้วยคำสั่ง
```bash
$ sudo /edx/bin/supervisorctl restart edxapp:
```

ตัวอย่างหน้าจอการใช้งาน  
![เมนูการใช้งาน](https://bitbucket.org/repo/887kgp/images/284792713-1-plugin-menu.jpg)

![แสดงผลการทำงาน](https://bitbucket.org/repo/887kgp/images/2430698061-2-searching.jpg)

![แสดงรายการของข้อมูลที่ค้นพบ](https://bitbucket.org/repo/887kgp/images/3103291438-3-object-list.jpg)

![ทำเครื่องหมายบนข้อมูลที่เลือก](https://bitbucket.org/repo/887kgp/images/3112704744-4-marked.jpg)

![ใส่ข้อมูลลงไปใน tinyMCE](https://bitbucket.org/repo/887kgp/images/1512147392-5-insert-to-editor.jpg)

![ข้อมูลที่แสดงในบทเรียน](https://bitbucket.org/repo/887kgp/images/3361672503-6-content-saved.jpg)