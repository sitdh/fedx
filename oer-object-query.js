$(document).ready(function(){

    currentPage = 1;
    q = '';

    inform = new SearchInform();
    objectFormatter = new OERFormatter();
    query = new OERQuery();
    selectedObjectList = new SelectedObject();

    formatter = new ObjectFormatter({
        'document': new DocumentObjectFormatter(),
        'image': new ImageObjectFormatter(),
        'video': new VideoObjectFormatter()
    });

    function isQueryChanged(changed) {
        c = $('#search-options-container');
        if ( undefined == changed ) {
            return ( 1 == c.data('isquerychanged') );
        } else if (typeof changed == "boolean" ) {
            c.data('isquerychanged', changed ? '1' : '0');
        }
    }

    function cleanupSearchResult() {
        resultWrapper = $('#resultimagewrapper');
        resultWrapper.empty();
    }

    inform.enableSearchInform();
    query.objectList(controlDataFetch(), function(data) {
        inform.disableAllMessage();
        cleanupSearchResult();

        if ( parseInt( data.total_item ) == 0 ) {
            infom.enableQueryInform();
        } else {
            c = controlDataFetch();
            objectFormatter.format( data, c.objectType );
        }
        objectFormatter.renderNavigationBar(data);
    });

    $('#q').keyup(function(e){
        if ( 13 == e.which ) {
            $('#query-object').click();
        }
    });

    $('#query-object').click(function(e){
        currentPage = 1;
        $('#footer-container').data('pagenumber', currentPage);
        selectedObjectList.removeAll();
        q = $('#q').val();
        cleanupSearchResult();
        inform.enableSearchInform();

        query.objectList(controlDataFetch(), function(data) {
            inform.disableAllMessage();
            if ( parseInt( data.total_item ) == 0 ) {
                inform.enableQueryInform();
            } else {
                objectFormatter.format( data );
            }
            objectFormatter.renderNavigationBar(data);
        });
    });

    $('#append-object').click(function(){
        if ( selectedObjectList.selectedObject.hasOwnProperty('length') && selectedObjectList.selectedObject.length > 0 ) {
            length = selectedObjectList.selectedObject.length;

            message = '';

            for( i = 0; i < length; i++ ) {
                id = selectedObjectList.selectedObject[i];
                o  = selectedObjectList.objects[id];
                if ( undefined == id || undefined == o ) continue;

                c = currentObjectType();
                message += formatter.format(o, c.objectType);
            }
            top.tinymce.activeEditor.execCommand('mceInsertContent', 0, message);
            top.tinymce.activeEditor.windowManager.close();
            selectedObjectList.removeAll();
        }

    });

    $('#q').change(function(){
        isQueryChanged(true);
    });


    $('#resultimagewrapper').on('click', '.resource-cover', function(){
        id = $(this).data('id');
        selectedObject = $('#oer-item-' + id);
        if ( selectedObject.hasClass('selected') ) {
            selectedObject.removeClass('selected');
            selectedObjectList.remove({
                'id': id,
            });
        } else {
            selectedObject.addClass('selected');
            o = controlDataFetch();
            selectedObjectList.append({
                'id': id ,
                'name': $(this).data('name'),
                'image': $(this).data('image'),
                'thumbnail': $(this).data('thumbnail'),
                'objectType': $(this).data('type')
            });
        }

    });

    $('.pagenav-selector').on('click', '.page-number-nav-number', function(){

        nav = $('.page-number-nav-number.selected')

        if ( ! $(this).hasClass('selected') && ( nav.length > 0 ) ) {
            nav.each(function(index){
                $(this).removeClass('selected');
            });
        }

        $('#footer-container').data('pagenumber', $(this).data('number'));
        info = controlDataFetch();
        info['needdle'] = q;

        if ( currentPage != info.pageNumber ) {
            currentPage = info.pageNumber;

            cleanupSearchResult();
            inform.enableSearchInform();

            query.objectList(info, function(data) {
                inform.disableAllMessage();

                if ( parseInt( data.total_item ) == 0 ) {
                    infom.enableQueryInform();
                } else {
                    c = controlDataFetch()
                    objectFormatter.format( data, c.objectType );
                }
            });
        }

        $(this).hasClass('selected') ? null : $(this).addClass('selected') ;
    });

});

function controlDataFetch() {

    return {
        needdle: $('#q').val(),
        objectType: $('#resulttype option:selected').val(),
        pageNumber: $('#footer-container').data('pagenumber')
    };

}

function OERQuery() {
    var query_endpoint = 'http://oer.learn.in.th/index.php/qapi/browse';

    this.objectList = function(option, cb) {
        var defaultOption = {
            objectType: 'document',
            pageNumber: 1,
            needdle: ''
        }

        $.extend(defaultOption, option);

        result_uri = this.uriBuilder(defaultOption);

        $.get(result_uri, function(data) {
            data = JSON.parse(data);
            if ( typeof cb == 'function' ) {
                cb(data);
            } else {
                return data;
            }
        });

    };

    this.getObjectURI = function(objectType) {

        object_uri = '';

        switch ( objectType ) {
            case 'document':
                object_uri = '/เอกสาร';
                break;
            case 'image':
                object_uri = '/รูปภาพ';
                break;
            case 'sound':
                object_uri = '/เสียง';
                break;
            case 'video':
                object_uri = '/ภาพเคลื่อนไหว';
                break;
            default:
                object_uri = '/ไม่ระบุ';
        }

        return query_endpoint + object_uri;
    };

    this.uriBuilder = function(option) {
        dataUri = this.getObjectURI(option.objectType);

        dataUri += "/" + option.pageNumber;

        if ( '' != option.needdle ) {
            dataUri += "/" + option.needdle;
        }

        return dataUri;
    };

}

function OERFormatter(wrap, isQueryChanged, pageNumber) {

    var o = null;
    var wrapper = wrap || 'result-box-wrapper';
    var totalItem = 0;
    var totalPage = 0;
    var query = "";
    var queryChanged = isQueryChanged || true;
    var currentPageNumber = pageNumber || 1;

    this.format = function(data, type) {
        type = type || 'image' ;
        if ( data.hasOwnProperty( 'resultset' ) ) this.renderFormattedObject(data.resultset, type);
    };

    this.renderFormattedObject = function(data, type) {

        type = type || 'image';

        $(data).each(function(index){
            if ( 'item' == this.type ) {

                objectWrapper = $('<div>', {
                    'data-objectid': this.id,
                    'id': 'oer-item-' + this.id,
                    'class': 'object-wrapper'
                });

                name = this.hasOwnProperty('Title') ? this['Title'] : this.name ;

                $('<div>', {
                    'class': 'resource-cover' ,
                    'data-id': this.id,
                    'data-image': this.url,
                    'data-thumbnail': this.image,
                    'data-name': name,
                    'data-type': type,
                    'title': name
                }).appendTo(objectWrapper);

                imageWrapper = $('<div>', { 'class': 'object-preview' } );

                icon_cover = $('<div>', {
                    'class': 'selected-mark'
                });

                $('<i>', {
                    'class': 'fa fa-check-circle-o'
                }).appendTo(icon_cover);

                icon_cover.appendTo(imageWrapper);

                $('<img>', {
                    'src': this.image,
                    'alt': name,
                    'onerror': "this.src='http://placehold.it/100x100/ba55d3/&text=X'"
                }).appendTo(imageWrapper);


                imageWrapper.appendTo(objectWrapper);

                $('<div>', {
                    'class': 'object-name',
                    'data-name': name
                }).text(name)
                .appendTo(objectWrapper);

                objectWrapper.appendTo(resultWrapper);

            }
        });

    }

    this.renderNavigationBar = function(nav, pageNumber) {
        pageNumber = $('#footer-container').data('pagenumber');
        total_page = nav['total_page'] || 0 ;
        navigationWrapper = $('#pagenav');
        navigationWrapper.empty();

        for( i = 1; i <= total_page; i++ ) {

            linkOption = {
                'data-number': i,
                'title': i,
                'class': 'page-number-nav page-number-nav-number'
            };
            linkOption['class'] += ( pageNumber == i ) ? ' selected' : '';

            linkList = $('<li>');
            link = $('<a>', linkOption)
                .text(i)
                .appendTo(linkList);

            linkList.appendTo(navigationWrapper);
        }

    };

}

function SelectedObject() {
    this.selectedObject = new Array();
    this.objects = { };

    this.append = function(o) {
        if ( false == this.objects.hasOwnProperty(o.id) ) {
            this.objects[o.id] = o;
            this.selectedObject.push(o.id);
        }

    }

    this.remove = function(o) {
        i = this.selectedObject.indexOf(o.id);
        if ( 0 <= i ) {
            delete this.selectedObject[i];
            delete this.objects[o.id];
        }
    }
    
    this.removeAll = function() {
        this.selectedObject = new Array();
        this.objects = { };
    }
}

function SearchInform() {
    searchControl = $('.info-message.searching');
    objectFoundControl = $('.info-message.object-not-found');

    this.disableAllMessage = function() {
        if ( searchControl.hasClass('active') ) searchControl.removeClass('active');
        if ( objectFoundControl.hasClass('active') ) objectFoundControl.removeClass('active');
    }

    this.enableSearchInform = function(enabled) {
        this.disableAllMessage();
        if ( undefined == enabled || ( ( typeof enabled == 'boolean' ) && enabled ) ) {
            searchControl.addClass('active');
        }
    }

    this.enableQueryInform = function(enabled) {
        this.disableAllMessage();
        if ( undefined == enabled || ( ( typeof enabled == 'boolean' ) && enabled ) ) {
            objectFoundControl.addClass('active');
        }
    }

}

function ObjectFormatter(support) {
    var support = (undefined == support) ? { } : support ;
    if ( ! support.hasOwnProperty('default') ) {
        $.merge(support, {
            'default': new DocumentObjectFormatter()
        });
    }

    this.format = function(o) {
        f = ( support.hasOwnProperty( o.objectType ) ) ? support[o.objectType] : support['default'] ;
        return f.parseHTML(o);
    }

}

function ImageObjectFormatter() {

    this.parseHTML = function(o) {

        oerWrapper = $('#object-template');
        oerWrapper.empty();

        $('<img>', {
            'alt': o.name,
            'src': o.image
        }).appendTo(oerWrapper);

        html = $(oerWrapper).html();
        oerWrapper.empty();

        return html;

    }
}

function DocumentObjectFormatter() {

    this.parseHTML = function(o) {

        oerWrapper = $('#object-template');
        oerWrapper.empty();

        link = $('<a>', {
            'href': o.image,
            'title': o.name,
            'target': '_blank' 
        });

        $('<img>', {
            'src': o.thumbnail,
            'alt': o.name
        }).appendTo(link);

        link.appendTo(oerWrapper);

        html = $(oerWrapper).html();
        oerWrapper.empty();

        return html;

    }
}

function VideoObjectFormatter() {

    this.parseHTML = function(o) {

        oerWrapper = $('#object-template');
        oerWrapper.empty();

        vdo = $('<video>', {
            'title': o.name,
            'controls': '',
            'poster': o.thumbnail,
        });

        $('<source>', {
            'src': o.image,
            'alt': o.name
        }).appendTo(vdo);

        vdo.appendTo(oerWrapper);

        html = $(oerWrapper).html();
        oerWrapper.empty();

        return html;

    }
}
